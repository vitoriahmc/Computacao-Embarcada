/**
 * 5 semestre - Eng. da Computação - Insper
 * Rafael Corsi - rafael.corsi@insper.edu.br
 *
 * Projeto 0 para a placa SAME70-XPLD
 *
 * Objetivo :
 *  - Introduzir ASF e HAL
 *  - Configuracao de clock
 *  - Configuracao pino In/Out
 *
 * Material :
 *  - Kit: ATMEL SAME70-XPLD - ARM CORTEX M7
 */

#include "asf.h"

/************************************************************************/
/* defines                                                              */
/************************************************************************/

#define LED_PIO PIOC
#define LED_PIO_ID 12
#define LED_PIO_PIN 8
#define LED_PIO_PIN_MASK (1 << LED_PIO_PIN)
#define BUT_PIO PIOA
#define BUT_PIO_ID 10
#define BUT_PIO_PIN 11
#define BUT_PIO_PIN_MASK (1 << BUT_PIO_PIN)
#define _PIO_DEFAULT (0u << 0)
#define _PIO_PULLUP (1u << 0)
#define _PIO_DEGLITCH (1u << 1)
#define _PIO_OPENDRAIN (1u << 2)
#define _PIO_DEBOUNCE (1u << 3)

/************************************************************************/
/* constants                                                            */
/************************************************************************/

/************************************************************************/
/* variaveis globais                                                    */
/************************************************************************/

/************************************************************************/
/* interrupcoes                                                         */
/************************************************************************/

/************************************************************************/
/* funcoes                                                              */
/************************************************************************/

/************************************************************************/
/* Main                                                                 */
/************************************************************************/
void _pio_set(Pio *p_pio, const uint32_t ul_mask){
	
	p_pio->PIO_SODR = ul_mask;
	
}

void _pio_clear(Pio *p_pio, const uint32_t ul_mask){
	
	p_pio->PIO_CODR = ul_mask;
	
}

void _pio_pull_up(Pio *p_pio, const uint32_t ul_mask, const uint32_t ul_pull_up_enable){
	if(ul_pull_up_enable==0){
		p_pio->PIO_PUDR = ul_mask;
	
	} else {
		p_pio->PIO_PUER = ul_mask;
	}
}

void _pio_set_output(Pio *p_pio, const uint32_t ul_mask, const uint32_t ul_default_level, const uint32_t ul_multidrive_enable, const uint32_t ul_pull_up_enable){
	p_pio->PIO_PER = ul_mask;
	p_pio->PIO_OER = ul_mask;
	
	if(ul_default_level==0){
		_pio_clear(p_pio, ul_mask);
	
	} else {
		_pio_set(p_pio, ul_mask);
	}
	
	if(ul_multidrive_enable==0){
		p_pio->PIO_MDDR = ul_mask;
	
	} else{
		p_pio->PIO_MDER = ul_mask;
	}
	
	_pio_pull_up(p_pio, ul_mask,ul_pull_up_enable );
}
	
void _pio_set_input(Pio *p_pio, const uint32_t ul_mask, const uint32_t ul_attribute){
	p_pio->PIO_PER = ul_mask;
	p_pio->PIO_PDSR = ul_mask;
	
	if(ul_attribute==_PIO_DEGLITCH){
		p_pio->PIO_IFSCDR = ul_mask;
	
	} else if(ul_attribute==_PIO_DEBOUNCE){
		p_pio->PIO_IFSCER = ul_mask;
	}
	
	if(ul_attribute == _PIO_PULLUP){
		_pio_pull_up(p_pio, ul_mask, 1);
	}
	
	if(ul_attribute == _PIO_OPENDRAIN){
			if(ul_multidrive_enable == 1){
				p_pio->PIO_MDER = ul_mask;
				} 
	}
}	

// Funcao principal chamada na inicalizacao do uC.
int main(void){
	//Initialize the board clock
	sysclk_init();
	
	
	//Desativa WatchDog
	WDT->WDT_MR = WDT_MR_WDDIS;
		
	//Ativa o controle do LED
	//pmc_enable_periph_clk(LED_PIO_ID);
	pmc_enable_periph_clk(BUT_PIO_ID);
	_pio_set_output(LED_PIO, LED_PIN_MASK,0);
	//pio_configure(BUT_PIO, PIO_INPUT, BUT_PIO_MASK, PIO_DEFAULT);
	_pio_pull_up(BUT_PIO, BUT_PIO_PIN_MASK, 1);
	_pio_set_input(BUT_PIO, BUT_PIO_PIN_MASK, _PIO_PULLUP);
	_pio_set(LED_PIO, LED_PIO_PIN_MASK);
	_pio_clear(LED_PIO, LED_PIO_PIN_MASK);
	
	while(1){

	int valor = pio_get(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK);
	
	if(valor == 0){
		for(int i = 0;i<5;i++){
			//Clear LED
			_pio_clear(LED_PIO, LED_PIO_PIN_MASK);
			
			delay_ms(200);
			
			//Set LED
			_pio_set(LED_PIO, LED_PIO_PIN_MASK);
			
			delay_ms(200);
			

		}
	
	} else{
			_pio_set(LED_PIO, LED_PIO_PIN_MASK);
	}
	
	}

	return 0;
}