#include <assert.h>
#include "asf.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "ioport.h"
#include "logo.h"

/** Reference voltage for AFEC,in mv. */
#define VOLT_REF        (3300)

/** The maximal digital value */
#define MAX_DIGITAL     (4095UL)

#define STRING_EOL    "\n"
#define STRING_HEADER "-- Temperature Sensor --\n\n" \
		"-- "BOARD_NAME" --\n\n" \
		"-- Compiled: "__DATE__" "__TIME__" --"STRING_EOL

#define SENSOR_AFEC         AFEC0
#define SENSOR_AFEC_ID      ID_AFEC0
#define SENSOR_AFEC_CH      AFEC_CHANNEL_5 // Pin PB2
#define SENSOR_AFEC_CH_IR   AFEC_INTERRUPT_EOC_5
#define SENSOR_THRESHOLD    50

/** The conversion data is done flag */
volatile bool is_conversion_done = false;

/** The conversion data value */
volatile uint32_t g_ul_value = 0;

struct ili9488_opt_t g_ili9488_display_opt;

void SENSOR_init(void);

void SENSOR_init(void){
	pmc_enable_periph_clk(SENSOR_AFEC_ID);
};

static void AFEC_Temp_callback(void)
{
	g_ul_value = afec_channel_get_value(SENSOR_AFEC, SENSOR_AFEC_CH);
	is_conversion_done = true;
};

static void configure_lcd(void){
	/* Initialize display parameter */
	g_ili9488_display_opt.ul_width = ILI9488_LCD_WIDTH;
	g_ili9488_display_opt.ul_height = ILI9488_LCD_HEIGHT;
	g_ili9488_display_opt.foreground_color = COLOR_CONVERT(COLOR_WHITE);
	g_ili9488_display_opt.background_color = COLOR_CONVERT(COLOR_WHITE);

	/* Initialize LCD */
	ili9488_init(&g_ili9488_display_opt);
	ili9488_draw_filled_rectangle(0, 0, ILI9488_LCD_WIDTH-1, ILI9488_LCD_HEIGHT-1);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_TOMATO));
	ili9488_draw_filled_rectangle(0, 0, ILI9488_LCD_WIDTH-1, 120-1);
	ili9488_draw_filled_rectangle(0, 360, ILI9488_LCD_WIDTH-1, 480-1);
	ili9488_draw_pixmap(0, 50, 319, 129, logoImage);
	
}

static void config_ADC_TEMP(void){
	afec_enable(AFEC0);

	/* struct de configuracao do AFEC */
	struct afec_config afec_cfg;

	/* Carrega parametros padrao */
	afec_get_config_defaults(&afec_cfg);

	/* Configura AFEC */
	afec_init(AFEC0, &afec_cfg);
  
	/* Configura trigger por software */
	afec_set_trigger(AFEC0, AFEC_TRIG_SW);
  
	/* configura call back */
	afec_set_callback(AFEC0, AFEC_INTERRUPT_EOC_5,	AFEC_Temp_callback, 1); 
   
	/*** Configuracao especfica do canal AFEC ***/
	struct afec_ch_config afec_ch_cfg;
	afec_ch_get_config_defaults(&afec_ch_cfg);
	afec_ch_cfg.gain = AFEC_GAINVALUE_0;
	afec_ch_set_config(AFEC0, SENSOR_AFEC_CH, &afec_ch_cfg);
  
	/*
	* Calibracao:
	* Because the internal ADC offset is 0x200, it should cancel it and shift
	 down to 0.
	 */
	afec_channel_set_analog_offset(AFEC0,SENSOR_AFEC_CH, 0x200);
	
	/* Selecina canal e inicializa converso */  
	afec_channel_enable(AFEC0, SENSOR_AFEC_CH);
		NVIC_SetPriority(AFEC0_IRQn, 10);
	afec_enable_interrupt(AFEC0, SENSOR_AFEC_CH);
	NVIC_EnableIRQ(AFEC0_IRQn);

}

static void configure_console(void)
{

  /* Configura USART1 Pinos */
  sysclk_enable_peripheral_clock(ID_PIOB);
  sysclk_enable_peripheral_clock(ID_PIOA);
  pio_set_peripheral(PIOB, PIO_PERIPH_D, PIO_PB4);  // RX
  pio_set_peripheral(PIOA, PIO_PERIPH_A, PIO_PA21); // TX
 	MATRIX->CCFG_SYSIO |= CCFG_SYSIO_SYSIO4;
 
	const usart_serial_options_t uart_serial_options = {
		.baudrate   = CONF_UART_BAUDRATE,
		.charlength = CONF_UART_CHAR_LENGTH,
		.paritytype = CONF_UART_PARITY,
		.stopbits   = CONF_UART_STOP_BITS,
	};

	/* Configure console UART. */
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);
}

int main(void)
{
	uint32_t ul_vol;
	float ul_temp;

	/* Initialize the SAM system. */
	sysclk_init();
	board_init();
	SENSOR_init();
	config_ADC_TEMP();

	configure_console();
	
	/* Initialize the SAM system */
	sysclk_init();
	
	/* Disable the watchdog */
	WDT->WDT_MR = WDT_MR_WDDIS;
	
	/* Output example information. */
	afec_start_software_conversion(AFEC0);
	
	// array para escrita no LCD
	uint8_t stingLCD[256];

    /* Escreve na tela Computacao Embarcada 2018 */
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
	ili9488_draw_filled_rectangle(0, 300, ILI9488_LCD_WIDTH-1, 315);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_BLACK));


	while (1) {

		if(is_conversion_done == true) {
			
			ul_vol = g_ul_value / MAX_DIGITAL;

			//ul_temp = ((float)ul_vol/10.0);
			
			ul_temp = ((float)ul_vol*1000.0); 

			printf("Temperatura atual: %4.1f \n", ul_temp);
			//sprintf(stingLCD, "%d Ohms", ul_temp);
			//ili9488_draw_string(10, 300, stingLCD);

			afec_start_software_conversion(AFEC0);
			is_conversion_done = false;			
		}
		afec_start_software_conversion(AFEC0);
		delay_ms(2500);
	}
}
